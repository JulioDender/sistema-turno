<?php

namespace App\Repository;

use App\Entity\Factura;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Factura>
 *
 * @method Factura|null find($id, $lockMode = null, $lockVersion = null)
 * @method Factura|null findOneBy(array $criteria, array $orderBy = null)
 * @method Factura[]    findAll()
 * @method Factura[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacturaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Factura::class);
    }

    public function save(Factura $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Factura $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function getFacturaCajero(): ?array
    {
        //se hace un left join para obtener las citas aun cuando no tengan registrado un medico Id
        $strSql = "SELECT facturas.id,
                   facturas.fe_creacion,
                   facturas.total,
                   facturas.estado,
                   turnos.descripcion descripcion,
                   userCajero.nombres cajero_nombres,
                   userCajero.apellidos cajero_apellidos
                   FROM App\Entity\Factura facturas
                   JOIN App\Entity\Usuario userCajero
                   WITH factura.cajero_id = userCajero.id
                   LEFT JOIN App\Entity\Turno tunos
                   WITH facturas.turno_id = turnos.id
                   WHERE facturas.estado =:estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado',"No Pagada")
                    ->getResult();         
    }public function getFacturaAdmin(): ?array
    {
        //se hace un left join para obtener las citas aun cuando no tengan registrado un medico Id
        $strSql = "SELECT facturas.id,
                   facturas.fe_creacion,
                   facturas.total,
                   facturas.estado,
                   citas.descripcion descripcion,
                   userCajero.nombres cajero_nombres,
                   userCajero.apellidos cajero_apellidos
                   FROM App\Entity\Factura facturas
                   JOIN App\Entity\User userCajero
                   WITH factura.cajero_id = userCajero.id
                   LEFT JOIN App\Entity\Cita citas
                   WITH facturas.cita_id = citas.id
                   WHERE facturas.estado =:estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado',"Pagada")
                    ->getResult();         
    }

//    /**
//     * @return Factura[] Returns an array of Factura objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Factura
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
