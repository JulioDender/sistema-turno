<?php

namespace App\Repository;

use App\Entity\Turno;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Turno>
 *
 * @method Turno|null find($id, $lockMode = null, $lockVersion = null)
 * @method Turno|null findOneBy(array $criteria, array $orderBy = null)
 * @method Turno[]    findAll()
 * @method Turno[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TurnoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Turno::class);
    }

    public function save(Turno $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Turno $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function getTurnoUsuario($idUser): ?array
    {
        //se hace un left join para obtener las turnos aun cuando no tengan registrado un medico Id
        $strSql = "SELECT turnos.id,
                   turnos.fecha,
                   turnos.descripcion,
                   turnos.estado,
                   tipoTurno.descripcion descripcionTipoTurno,
                   userMedico.nombres nombre_medico,
                   userMedico.apellidos apellido_medico
                   FROM App\Entity\Turno turnos
                   LEFT JOIN App\Entity\Usuario userMedico
                   WITH turnos.medico_id = userMedico.id
                   LEFT JOIN App\Entity\TipoTurno tipoTurno
                   WITH turnos.tipo_id = tipoTurno.id
                   WHERE turnos.paciente_id = :paciente";
        return $this->_em->createQuery($strSql)
                    ->setParameter('paciente',$idUser)
                    ->getResult();         
    }
    public function getTurnoMedico($idUser): ?array
    {
        //se hace un left join para obtener las turnos aun cuando no tengan registrado un medico Id
        $strSql = "SELECT turnos.id,
                   turnos.fecha,
                   turnos.descripcion,
                   turnos.estado,
                   turnos.observacion,
                   tipoTurno.descripcion descripcionTipoTurno,
                   tipoTurno.precio valorTurno,
                   userMedico.nombres nombre_medico,
                   userMedico.apellidos apellido_medico,
                   userPaciente.nombres paci_nombres,
                   userPaciente.apellidos paci_apellidos
                   FROM App\Entity\Turno turnos
                   JOIN App\Entity\Usuario userPaciente
                   WITH turnos.paciente_id = userPaciente.id
                   LEFT JOIN App\Entity\Usuario userMedico
                   WITH turnos.medico_id = userMedico.id
                   LEFT JOIN App\Entity\TipoTurno tipoTurno
                   WITH turnos.tipo_id = tipoTurno.id
                   WHERE turnos.medico_id =:medico";
        return $this->_em->createQuery($strSql)
                    ->setParameter('medico',$idUser)
                    ->getResult();         
    }
    public function getTurnoEstado($estado): ?array
    {
        //se hace un left join para obtener las turnos aun cuando no tengan registrado un medico Id
        $strSql = "SELECT turnos.id,
                   turnos.fecha,
                   turnos.descripcion,
                   turnos.estado,
                   tipoTurno.descripcion descripcionTipoTurno,
                   tipoTurno.precio valorTurno,
                   userMedico.nombres nombre_medico,
                   userMedico.apellidos apellido_medico,
                   userPaciente.nombres paci_nombres,
                   userPaciente.apellidos paci_apellidos
                   FROM App\Entity\Turno turnos
                   JOIN App\Entity\Usuario userPaciente
                   WITH turnos.paciente_id = userPaciente.id
                   LEFT JOIN App\Entity\Usuario userMedico
                   WITH turnos.medico_id = userMedico.id
                   LEFT JOIN App\Entity\TipoTurno tipoTurno
                   WITH turnos.tipo_id = tipoTurno.id
                   WHERE turnos.estado =:estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado',$estado)
                    ->getResult();         
    }
    

//    /**
//     * @return Turno[] Returns an array of Turno objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Turno
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
