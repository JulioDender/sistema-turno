<?php

namespace App\Controller;

use App\Repository\FacturaRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContadorDashboardController extends AbstractController
{
    #[Route('/contador/dashboard', name: 'app_contador_dashboard')]
    public function index(Request $request,FacturaRepository $facturaRepository): Response
    {
        return $this->render('admin_dashboard/index.html.twig', [
            'listFacturas'=>$facturaRepository->getFacturaAdmin(),
        ]);
    }
}
