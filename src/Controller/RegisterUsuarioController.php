<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Form\RegisterUserType;
use App\Form\RegisterMedicoType;
use App\Repository\UsuarioRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class RegisterUsuarioController extends AbstractController
{
    #[Route('/register/usuario', name: 'app_register_usuario')]
    public function index(Request $request, UserPasswordHasherInterface $encoderPassword, ManagerRegistry $doctrine, UsuarioRepository $userRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $usuario = $userRepository->findOneBy(["email"=>$email]);
        $user = new Usuario(USUARIO ::ROLE_USER ,"Tester");
        
            $form = $this -> createForm(RegisterUserType :: class, $user);
            $form -> handleRequest($request);
            $user ->setTipo("Paciente");
        
        
        if($form ->isSubmitted() && $form ->isValid())
        {
            $user=$form ->getData();
            $user -> setPassword($encoderPassword ->hashPassword($user,$form["password"]->getData()));
            /*$user->setClave(
                $encoderPassword->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );*/
            if ($user->getTipo() == "Admin")
            {
                $user -> setRoles([USUARIO ::ROLE_ADMIN]);
            }
            elseif ($user->getTipo() == "Medico" )
            {
                $user -> setRoles([USUARIO ::ROLE_MEDICO]);
            }
            elseif ($user->getTipo() == "Enfermero" )
            {
                $user -> setRoles([USUARIO ::ROLE_ENFERMERO]);
            }
            elseif ($user->getTipo() == "Contador" )
            {
                $user -> setRoles([USUARIO ::ROLE_CONTADOR]);
            }
            elseif ($user->getTipo() == "Cajero" )
            {
                $user -> setRoles([USUARIO ::ROLE_CAJERO]);
            }
            elseif ($user->getTipo() == "Paciente" )
            {
                $user -> setRoles([USUARIO ::ROLE_PACIENTE]);
            }
            
            $userRepository->save($user,true);    
            return $this-> redirectToRoute('app_login');
                
            
        }
        return $this->render('register_usuario/index.html.twig', [
            'formulario' => $form ->createView(),
        ]);
    }
    #[Route('/register/usuario1', name: 'app_register_usuario1')]
    public function index1(Request $request, UserPasswordHasherInterface $encoderPassword, ManagerRegistry $doctrine, UsuarioRepository $userRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $usuario = $userRepository->findOneBy(["email"=>$email]);
        $user = new Usuario(USUARIO ::ROLE_USER ,"Tester");
            $form = $this -> createForm(RegisterUserType :: class, $user,['accion'=>'crearAdmin']);
            $form -> handleRequest($request);
        
       
        
        if($form ->isSubmitted() && $form ->isValid())
        {
            $user=$form ->getData();
            $user -> setPassword($encoderPassword ->hashPassword($user,$form["password"]->getData()));
            /*$user->setClave(
                $encoderPassword->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );*/
            if ($user->getTipo() == "Admin")
            {
                $user -> setRoles([USUARIO ::ROLE_ADMIN]);
            }
            elseif ($user->getTipo() == "Medico" )
            {
                $user -> setRoles([USUARIO ::ROLE_MEDICO]);
            }
            elseif ($user->getTipo() == "Enfermero" )
            {
                $user -> setRoles([USUARIO ::ROLE_ENFERMERO]);
            }
            elseif ($user->getTipo() == "Contador" )
            {
                $user -> setRoles([USUARIO ::ROLE_CONTADOR]);
            }
            elseif ($user->getTipo() == "Cajero" )
            {
                $user -> setRoles([USUARIO ::ROLE_CAJERO]);
            }
            elseif ($user->getTipo() == "Paciente" )
            {
                $user -> setRoles([USUARIO ::ROLE_PACIENTE]);
            }
            
            $userRepository->save($user,true);    
            return $this-> redirectToRoute('app_admin_dashboard');
                
            
        }
        return $this->render('register_usuario/index.html.twig', [
            'formulario' => $form ->createView(),
        ]);
    }
    #[Route('/register/usuario2', name: 'app_register_usuario2')]
    public function index2(Request $request, UserPasswordHasherInterface $encoderPassword, ManagerRegistry $doctrine, UsuarioRepository $userRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $usuario = $userRepository->findOneBy(["email"=>$email]);
        $user = new Usuario(USUARIO ::ROLE_USER ,"Tester");
            $form = $this -> createForm(RegisterMedicoType :: class, $user);
            $form -> handleRequest($request);
        if($form ->isSubmitted() && $form ->isValid())
        {
            $user=$form ->getData();
            $user -> setPassword($encoderPassword ->hashPassword($user,$form["password"]->getData()));
            /*$user->setClave(
                $encoderPassword->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );*/
                $user-> setTipo("Medico");
                $user -> setRoles([USUARIO ::ROLE_MEDICO]);
            
            
            $userRepository->save($user,true);    
            return $this-> redirectToRoute('app_admin_dashboard');
                
            
        }
        return $this->render('register_usuario/index1.html.twig', [
            'formulario' => $form ->createView(),
        ]);
    }
}
