<?php

namespace App\Controller;

use App\Entity\Factura;
use App\Repository\FacturaRepository;
use App\Repository\UsuarioRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CajeroDashboardController extends AbstractController
{
    #[Route('/cajero/dashboard', name: 'app_cajero_dashboard')]
    public function index(Request $request,FacturaRepository $facturaRepository): Response
    {
        
        return $this->render('cajero_dashboard/index.html.twig', [
            'listFacturas'=>$facturaRepository->getFacturaCajero(),
        ]);
    }
    #[Route('/cajero/dashboard/cobrar/{id}', name: 'app_cajerp_dashboard_cobrar')]
    public function cobrar(Request $request,Factura $factura,FacturaRepository $facturaRepository,UsuarioRepository $userRepository,ManagerRegistry $doctrine): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        $objUsuario = $userRepository->findOneBy(["email"=>$email]);
        $factura->setCajeroId($objUsuario->getId());
        $factura->setEstado("Pagada");
        return $this->redirectToRoute('app_cajero_dashboard');
    }
}
