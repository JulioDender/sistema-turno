<?php

namespace App\Controller;

use App\Entity\Turno;
use App\Form\RegisterTurnoType;
use App\Repository\TurnoRepository;
use App\Repository\UsuarioRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PacienteDashboardController extends AbstractController
{
    #[Route('/paciente/dashboard', name: 'app_paciente_dashboard')]
    public function index(Request $request, TurnoRepository $turnoRepository, UsuarioRepository $UsuarioRepository): Response
    {
        //$user = $request ->getUser();
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $UsuarioRepository->findOneBy(["email"=>$email]);
        return $this->render('paciente_dashboard/index.html.twig', [
            'listTurnos'=>$turnoRepository->getTurnoUsuario($user->getId()),
        ]);
    }
    #[Route('/paciente/dashboard/register', name: 'app_paciente_dashboard_register')]
    public function agregar(Request $request, TurnoRepository $turnoRepository,ManagerRegistry $doctrine, UsuarioRepository $UsuarioRepository): Response
    {
        //$user = $request->request ->ge()
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $UsuarioRepository->findOneBy(["email"=>$email]);
        $turno=new Turno();
        $form =$this->createForm(RegisterTurnoType::class, $turno);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $turno=$form->getData();
            $turno->setPacienteId($user->getId());
            $turno->setEstado("En Espera");
            //$turno->setDuracion(30);
            //$turno ->setFeCreacion(new \Datetime());
            $em=$doctrine->getManager();
            $em->persist($turno);
            $em->flush();
            $this->addFlash("success","Registro de Turno Exitoso");
            return $this->redirectToRoute('app_paciente_dashboard');
        }
        return $this->render('paciente_dashboard/registerTurno.html.twig', [
            'formulario'=>$form->createView(),
        ]);
    }

    #[Route('/paciente/dashboard/edit/{id}', name: 'app_paciente_dashboard_edit')]
    public function edit(Request $request,Turno $turno, TurnoRepository $turnoRepository,ManagerRegistry $doctrine, UsuarioRepository $UsuarioRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        
        $objUsuario = $UsuarioRepository->findOneBy(["email"=>$email]);
        $form = $this->createForm(RegisterTurnoType::class, $turno);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if($turno->getEstado() == "En Espera")    
        {
            $turno=$form->getData();
            $turno->setPacienteId($objUsuario->getId());
           // $turno->setEstado("En Espera");
            $this->addFlash("success", "Exitos: El turno fue editado con exito");
            $turnoRepository->save($turno,true);
            
        }
        else 
        {
            $this->addFlash("Error", "Error: No puede eliminar un turno que ya se encuentra agendado"); 
        }
          return $this->redirectToRoute('app_paciente_dashboard');  
        }
        return $this->render('paciente_dashboard/registerTurno.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }

    #[Route('/paciente/dashboard/delete/{id}', name: 'app_paciente_dashboard_delete')]
    public function delete(Turno $turno, TurnoRepository $turnoRepository): Response
    {
        if($turno->getEstado() == "En Espera")    
        {
            $turno-> setEstadoBase("I");
           
            
        }
        else 
        {
            $this->addFlash("error", "Error: No puede eliminar un juego que este siendo testeado"); 
        }
        
        return $this->redirectToRoute('app_paciente_dashboard');
    }
}
