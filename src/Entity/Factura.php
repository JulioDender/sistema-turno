<?php

namespace App\Entity;

use App\Repository\FacturaRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FacturaRepository::class)]
class Factura
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $fecha = null;

    #[ORM\Column]
    private ?float $total = null;

    #[ORM\Column(length: 25)]
    private ?string $estado = null;

    #[ORM\Column(length: 1, nullable: true)]
    private ?string $estado_base = null;

    #[ORM\Column(nullable: true)]
    private ?int $cajero_id = null;

    #[ORM\Column(nullable: true)]
    private ?int $paciente_id = null;

    #[ORM\Column]
    private ?int $turno_id = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getEstadoBase(): ?string
    {
        return $this->estado_base;
    }

    public function setEstadoBase(?string $estado_base): self
    {
        $this->estado_base = $estado_base;

        return $this;
    }

    public function getCajeroId(): ?int
    {
        return $this->cajero_id;
    }

    public function setCajeroId(?int $cajero_id): self
    {
        $this->cajero_id = $cajero_id;

        return $this;
    }

    public function getPacienteId(): ?int
    {
        return $this->paciente_id;
    }

    public function setPacienteId(?int $paciente_id): self
    {
        $this->paciente_id = $paciente_id;

        return $this;
    }

    public function getTurnoId(): ?int
    {
        return $this->turno_id;
    }

    public function setTurnoId(int $turno_id): self
    {
        $this->turno_id = $turno_id;

        return $this;
    }
}
